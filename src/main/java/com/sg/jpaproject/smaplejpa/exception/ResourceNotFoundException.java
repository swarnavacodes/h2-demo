package com.sg.jpaproject.smaplejpa.exception;

public class ResourceNotFoundException extends Exception{

    private static final long serialVersionUUID = 1L;

    public ResourceNotFoundException(String message) {
        super (message);
    }
}
