package com.sg.jpaproject.smaplejpa.controller;

import com.sg.jpaproject.smaplejpa.exception.ResourceNotFoundException;
import com.sg.jpaproject.smaplejpa.model.Monitor;
import com.sg.jpaproject.smaplejpa.repository.MonitorRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class MonitorController {

    @Autowired
    private MonitorRepository monitorRepository;

    @GetMapping("/eprid/{id}")
    public ResponseEntity<?> getMonitorByEprId(@PathVariable(value = "id") long eprId) {
        try {
            Monitor monitor = monitorRepository.findById(eprId)
                    .orElseThrow(() -> new ResourceNotFoundException("Application not found for this id :: " + eprId));
            return ResponseEntity.ok().body(monitor);
        } catch (ResourceNotFoundException ex) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ErrorResponse(ex.getMessage()));
        }
    }

    @Data
    @AllArgsConstructor
    public static class ErrorResponse {
        private String message;
    }
}
