package com.sg.jpaproject.smaplejpa.repository;

import com.sg.jpaproject.smaplejpa.model.Monitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitorRepository extends JpaRepository<Monitor, Long> {
}
