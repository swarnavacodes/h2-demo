package com.sg.jpaproject.smaplejpa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name="appmonitor")
@Accessors(chain=true)
public class Monitor {

    @Id
    @Column(name = "id", nullable = false)
    private long id;
    @Column(name = "epr_id", nullable = true)
    private long eprId;
    @Column(name= "application", nullable = true )
    private String application;
    @Column(name= "status", nullable = true )
    private String stats;

}
